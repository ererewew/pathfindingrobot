//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Robot.rc
//
#define IDC_MYICON                      2
#define IDD_ROBOT_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_ROBOT                       107
#define IDI_SMALL                       108
#define IDC_ROBOT                       109
#define IDR_MAINFRAME                   128
#define IDD_INPUTPOINT                  129
#define IDD_WAITING                     130
#define IDR_MENU1                       131
#define IDC_EDITX                       1000
#define IDC_EDIT2                       1001
#define IDC_EDITY                       1001
#define ID_LOADMAP                      32771
#define ID_EXECUTEALGORITHM_ITERATIVEDEEPENINGA 32772
#define ID_EXECUTEALGORITHM_RECURSIVEBESTFIRSTSEARCH 32773
#define ID_EXECUTEALGORITHM_A           32774
#define ID_IDASTAR                      32775
#define ID_RBFSEARCH                    32776
#define ID_ASTAR                        32777
#define ID_EDIT_SETSOURCEPOINT          32778
#define ID_EDIT_SETDESTINATIONPOINT     32779
#define ID_EDIT_SETDESTPOINT            32780
#define ID_REFRESH                      32781
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
