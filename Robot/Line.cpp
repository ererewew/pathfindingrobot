#include "stdafx.h"
#include "Line.h"
#include "Polygon.h"
Line::Line(const MYPOINT & a, const MYPOINT & b)
{
	int vectX = -(b.y - a.y);
	int vectY = b.x - a.x;
	_coefA = vectX;
	_coefB = vectY;
	_coefC = vectX * (-a.x) + vectY * (-a.y);
}
/*
	Construct Line Equation from two points
	Input: Two Points
	Output:
		Return true if the function sucessfully set Line Equation
		Return false if there is an error
	P/s: This function guarantee nothing change when error happens
*/
bool Line::setLineEquation(const MYPOINT & a, const MYPOINT & b)
{
	if (compare(a.x, b.x) == 0 && compare(a.y, b.y) == 0)
		return false;
	int vectX = -(b.y - a.y);
	int vectY = b.x - a.x;
	_coefA = vectX;
	_coefB = vectY;
	_coefC = vectX * (-a.x) + vectY * (-a.y);
	return true;
}

Line::Line() : _coefA(), _coefB(), _coefC()
{
}
const MYPOINT & LineSegment::getMYPOINTA()
{
	// TODO: insert return statement here
	return this->_a;
}
const MYPOINT & LineSegment::getMYPOINTB()
{
	// TODO: insert return statement here
	return this->_b;
}
/*
	LineSegment default constructor
*/
LineSegment::LineSegment() : _a{}, _b{}
{
}
/*
	This function will check the relationship between two line segments
	Input: Another line segment you want to check, A MYPOINT to receive intersection MYPOINT
	Output:
		Return 1 if two lines segments intersects at only one MYPOINT
		Return 2 if two lines are colinear and overlapped
		Return 0 if two lines doesn't fall in cases above
		Return the intersect MYPOINT if any

*/
int LineSegment::getRelates(const LineSegment & line, MYPOINT& result)
{
	int crossProductA = CrossProduct(_a, _b, line._a, line._b);
	int crossProductB = CrossProduct(_a, line._a, _a, _b);
	if (crossProductA == 0 && crossProductB == 0)
	{
		double t0 = (double)DotProduct(_a, line._a, _a, _b) / (double)DotProduct(_a, _b, _a, _b);
		MYPOINT tmp{};
		double t1 = t0 + (double)DotProduct(tmp, _a, _a, _b) / (double)DotProduct(_a, _b, _a, _b);
		if (DotProduct(tmp, _a, _a, _b) < 0)
		{
			if (t1 >= 0 && t1 <= 1)
			{
				return 2;
			}
			else
				return 0;
		}
		else
		{
			if (t0 >= 0 && t0 <= 1)
				return 2;
			else
				return 0;
		}
	}
	else if (crossProductA == 0 && crossProductB != 0)
	{
		return 0;
	}
	else if (crossProductA != 0)
	{
		double t = (double)CrossProduct(_a, line._a, line._a, line._b) / CrossProduct(_a, _b, line._a, line._b);
		double u = (double)CrossProduct(line._a, _a, _a, _b) / CrossProduct(line._a, line._b, _a, _b);
		if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
		{
			result.x = _a.x + t * (_b.x - _a.x);
			result.y = _a.y + t * (_b.y - _a.y);
			return 1;
		}
		return 0;
	}
	return 0;
}

/*
	This function will check if this line segment will cross the polygon
	Input: Polygon
	Output:
		Return 1 if the line segment cross the polygon
		Return 0 otherwise
		Return -1 if error happens
*/
int LineSegment::crossPolygon(const POLYGON & poly)
{
	LineSegment lineSeg;
	MYPOINT intersectA{ -1,-1 };
	MYPOINT intersectB{ -1,-1 };
	int output;
	int i = 0;
	//Check line segment (v0,vn-1)
	lineSeg.setEndPoints(poly._vertList[0]->_point, poly._vertList[poly._vertList.size() - 1]->_point);
	output = this->getRelates(lineSeg, intersectA);
	if (output == 2)
	{
		return 0;
	}
	for (; i < poly._vertList.size() - 1; ++i)
	{
		if (lineSeg.setEndPoints(poly._vertList[i]->_point, poly._vertList[i + 1]->_point))
		{
			if (intersectA.x == -1)
				output = this->getRelates(lineSeg, intersectA);
			else
				output = this->getRelates(lineSeg, intersectB);

			if (output == 2)
			{
				//if two lines are overlapped, return false immediately because this is convex polygon
				return 0;
			}
			else if (output == 1)
			{
				if (intersectB.x != -1)
				{
					//There are two insections
					//If they are the same, reset intersectB, in this case, intersectB must be a vertex
					if (intersectA.x == intersectB.x && intersectA.y == intersectB.y)
					{
						intersectB.x = -1;
						intersectB.y = -1;
					}
					else
					{
						//If they are not, line segments cross the polygon
						return 1;
					}
				}
			}
		}
		else
			return -1;
	}
	return 0;
}
/*
	This functions will set two end points of the line segment
	Line equation will also change
	Input: Two points
	Output:
		Return true if two points are set sucessfully
		Return false if there is an error
	P/s: This function will guarantee to be unchanged if there is an error
*/
bool LineSegment::setEndPoints(const MYPOINT & a, const MYPOINT & b)
{
	if (compare(a.x, b.x) == 0 && compare(a.y, b.y) == 0)
		return false;
	if (Line::setLineEquation(a, b))
	{
		this->_a.x = a.x;
		this->_a.y = a.y;
		this->_b.x = b.x;
		this->_b.y = b.y;
		return true;
	}
	return false;
}

/*
	This function will create a LineSegment from two MYPOINTs along with the line equation
	Input: MYPOINT a, MYPOINT b
	Output: The Line Segment, and its Line Equation, if error happens, nullptr will be returned
	P/s: The Line Segment is dynamically allocated, delete it if you finish.
*/
LineSegment* LineSegment::createLineSegment(const MYPOINT & a, const MYPOINT & b)
{
	if (compare(a.x, b.x) == 0 && compare(a.y, b.y) == 0)
		return nullptr;
	LineSegment* lineSeg = new LineSegment;
	if (lineSeg == nullptr)
		return nullptr;
	lineSeg->_a = a;
	lineSeg->_b = b;
	lineSeg->setLineEquation(a, b);
	return lineSeg;
}