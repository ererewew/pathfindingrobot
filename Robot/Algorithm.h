#pragma once
#include "stdafx.h"
#define FOUND -1
#define NOT_FOUND -2
class Map;
struct POLYPOINT;
struct POLYPOINTRBFS
{
	POLYPOINT* _polyPoint;
	double _f;
};
//This is where you implement algorithm
//Every algorithm has wrapper functions
//Wrapper prototype: int AlgoName(const Map& map,double& distance);
class Algorithm
{
public:
	static int IDAStar (Map& map,double& distance,std::vector<POLYPOINT*>& path);
	static double IDAStarSearch(std::vector<POLYPOINT*>& path, double realCost, double cutOff, Map& map);
	static int AStar(Map& map, double& cost, std::vector<POLYPOINT*>& path);
	static int RecurBestFirstSearch(Map& map, double& distance, std::vector<POLYPOINT*>& path);
	static double RecurBFSUtil(std::vector<POLYPOINT*> &path,std::shared_ptr<POLYPOINTRBFS>& node,double realCost, double bestValue, Map& map);
};