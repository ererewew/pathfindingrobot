#include "stdafx.h"
#include "Map.h"
#include "Line.h"
Map::Map()
{
}

Map::~Map()
{
	for (auto i : _polyList)
	{
		delete i;
	}
}

bool Map::loadMap(const char * filename)
{
	std::ifstream fin(filename);
	if (fin.is_open())
	{
		_polyList.clear();
		int srcX, srcY, destX, destY;
		fin.get();
		fin >> srcX;
		fin.get();
		fin >> srcY;
		fin.get();
		fin.get();
		fin.get();
		fin >> destX;
		fin.get();
		fin >> destY;
		fin.get();
		fin.get();
		updateConnectedListSize();
		MYPOINT pt;
		pt.x = srcX;
		pt.y = srcY;
		setSource(pt);
		pt.x = destX;
		pt.y = destY;
		setDest(pt);
		if (fin.fail())
			return true;
		int i = 0, j = 0;
		while(!fin.fail() || !fin.eof())
		{
			auto plygn = new POLYGON();
			while(fin.get()!='\n' && !fin.fail())
			{
				POLYPOINT* pt = new POLYPOINT();
				fin >> pt->_point.x;
				fin.get();
				fin >> pt->_point.y;
				pt->_tag.polyTag = i;
				pt->_tag.vertTag = j;
				fin.get();
				plygn->addVert(pt);
				++j;
			}
			if (!fin.fail())
			{
				addPolygon(plygn);
				++i;
				j = 0;
			}
			else
				delete plygn;
			
		}
		this->updateConnectedListSize();
		pt.x = srcX;
		pt.y = srcY;
		setSource(pt);
		pt.x = destX;
		pt.y = destY;
		setDest(pt);
		
	}
	fin.close();
	return true;
}
/*
	This function will set the Source of the map
	If the source is polygon vertex, _srcConnectedList = that polygon vertex's connected list
*/
bool Map::setSource(const MYPOINT & src)
{
	this->_src._point = src;
	this->_src._tag.polyTag = this->_polyList.size();
	this->_src._tag.vertTag = 0;
	for (int i = 0; i < _polyList.size(); ++i)
	{
		if (_polyList[i]->isPtInside(src))
		{
			this->isValidSource = false;
			return false;
		}
	}
	isValidSource = true;
	return true;
}

bool Map::setDest(const MYPOINT & dest)
{

	this->_dest._point = dest;
	this->_dest._tag.polyTag = this->_polyList.size() + 1;
	this->_dest._tag.vertTag = 0;
	for (int i = 0; i < _polyList.size(); ++i)
	{
		if (_polyList[i]->isPtInside(dest))
		{
			this->isValidDest = false;
			break;
		}
	}
	isValidDest = true;
	return true;
}

void Map::Run(HWND hWnd,Map & map, MyAlgorithm algo)
{
	map.clearPath();
	time_t startT = clock();
	if (map.isValidSource && map.isValidDest)
	{
		int result = algo(map, map.distance, map.path);
		if (result == -1)
		{

		}
		else
		{
			MessageBoxA(hWnd, "There is no solution", "Result", MB_OK);
		}
	}
	else
		MessageBoxA(hWnd, "There is no solution", "Result", MB_OK);
	auto endT = clock();
	map.execTime = endT - startT;
}

/*

*/
std::shared_ptr<std::list<TAG>> Map::generateNodes(POLYPOINT * node) const
{
	auto polyTag = node->_tag.polyTag;
	auto vertTag = node->_tag.vertTag;
	auto maxPoly = this->_polyList.size();
	auto curList = this->_connectedList[polyTag][vertTag];
	bool crossAny = false;
	if (curList)
		return curList;
	curList = std::shared_ptr<std::list<TAG>>(new std::list<TAG>);
	//Check if _dest is legal successor
	LineSegment lineSeg;
	if (lineSeg.setEndPoints(node->_point, _dest._point))
	{
		for (int k = 0; k < _polyList.size(); ++k)
		{
			if (lineSeg.crossPolygon(*_polyList[k]) == 1)
			{
				crossAny = true;
				break;
			}
		}
	}
	if (!crossAny)
	{
		TAG tag;
		tag.polyTag = _dest._tag.polyTag;
		tag.vertTag = _dest._tag.vertTag;
		curList.get()->push_back(tag);
	}
	//If this point is polygon vertex, add two points that next to it
	if (polyTag < maxPoly)
	{
		TAG tag;
		tag.polyTag = polyTag;
		tag.vertTag = vertTag + 1 >= _polyList[polyTag]->_vertList.size() ? 0 : vertTag + 1;
		if(_polyList[polyTag]->_vertList[tag.vertTag]->_point.x != _dest._point.x || _polyList[polyTag]->_vertList[tag.vertTag]->_point.y != _dest._point.y)
			curList.get()->push_back(tag);
		tag.vertTag = vertTag - 1 < 0 ? _polyList[polyTag]->_vertList.size() - 1 : vertTag - 1;
		if (_polyList[polyTag]->_vertList[tag.vertTag]->_point.x != _dest._point.x || _polyList[polyTag]->_vertList[tag.vertTag]->_point.y != _dest._point.y)
			curList.get()->push_back(tag);
	}
	crossAny = false;
	//For all the polygons in the map, check for legal successors
	for (int i = 0; i < _polyList.size(); ++i)
	{
		if (i != polyTag)
		{
			for (int j = 0; j < _polyList[i]->_vertList.size(); ++j)
			{
				if (_polyList[i]->_vertList[j]->_point.x == _dest._point.x && _polyList[i]->_vertList[j]->_point.y == _dest._point.y)
					continue;
				if (lineSeg.setEndPoints(node->_point, _polyList[i]->_vertList[j]->_point))
				{
					for (int k = 0; k < _polyList.size(); ++k)
					{
						if (lineSeg.crossPolygon(*_polyList[k]) == 1)
						{
							crossAny = true;
							break;
						}
					}
					if (!crossAny)
					{
						TAG tag;
						tag.polyTag = i;
						tag.vertTag = j;
						curList->push_back(tag);
					}
					crossAny = false;

				}
			}
		}
	}
	return curList;
}

void Map::addPolygon(POLYGON * polygon)
{
	_polyList.push_back(polygon);
}

void Map::updateConnectedListSize()
{
	_connectedList.resize(0);
	_connectedList.resize(_polyList.size() + 2);
	for (int i = 0; i < _polyList.size(); ++i)
		_connectedList[i].resize(_polyList[i]->getNumVert());
	_connectedList[_polyList.size()].resize(1);
	_connectedList[_polyList.size() + 1].resize(1);
}

POLYPOINT Map::getDest() const
{
	return _dest;
}

POLYPOINT Map::getSource() const
{
	return _src;
}

POLYPOINT Map::getPoint(int pPolyTag, int pVertTag)
{
	if (pPolyTag == _polyList.size())
		return _src;
	if (pPolyTag == _polyList.size() + 1)
		return _dest;
	return _polyList[pPolyTag]->getPoint(pVertTag);
}

POLYPOINT * Map::getPointPtr(int pPolyTag, int pVertTag)
{
	if (pPolyTag == _polyList.size())
		return &_src;
	if (pPolyTag == _polyList.size() + 1)
		return &_dest;
	return _polyList[pPolyTag]->_vertList[pVertTag];
}

BOOL Map::isReachDest()
{
	if (this->path.empty())
		return FALSE;
	if (this->path.back()->_point.x == this->_dest._point.x && this->path.back()->_point.y == this->_dest._point.y)
		return TRUE;
	return FALSE;
}

void Map::draw(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	RECT rect;
	GetClientRect(hWnd, &rect);
	SetViewportOrgEx(hdc, 0, rect.bottom, NULL);
	auto paddingMaxX = maxX + RATIO*maxX;
	auto paddingMaxY = maxY + RATIO*maxY;
	
	auto yellowBrush = CreateSolidBrush(RGB(255, 255, 0));
	auto oldBrush = SelectObject(hdc, yellowBrush);
	for (int i = 0; i < this->_polyList.size(); ++i)
	{
		std::vector<POINT> arrPt;
		for (int j = 0; j < this->_polyList[i]->_vertList.size(); ++j)
		{
			auto polyPt = _polyList[i]->_vertList[j];
			POINT pt;
			pt.x = (polyPt->_point.x*scalingRatio + PADDING) ;
			pt.y = -(polyPt->_point.y*scalingRatio + PADDING) ;
			arrPt.push_back(pt);
		}
		Polygon(hdc, arrPt.data(), arrPt.size());
		auto iterA = std::max_element(arrPt.begin(), arrPt.end(),
			[](POINT& a, POINT& b) {
			return a.x < b.x;
		});
		auto iterB = std::min_element(arrPt.begin(), arrPt.end(),
			[](POINT& a, POINT& b) {
			return a.x < b.x;
		});
	}
	auto pinkBrush = CreateSolidBrush(RGB(232, 64, 70));
	SelectObject(hdc, pinkBrush);
	//Draw source and dest
	Ellipse(hdc, (_src._point.x*scalingRatio - POINT_RAD + PADDING),
		-(_src._point.y*scalingRatio - POINT_RAD + PADDING),
		(_src._point.x*scalingRatio + POINT_RAD+ PADDING),
		-(_src._point.y*scalingRatio + POINT_RAD + PADDING));
	Ellipse(hdc, (_dest._point.x*scalingRatio - POINT_RAD + PADDING),
		-(_dest._point.y*scalingRatio - POINT_RAD + PADDING),
		(_dest._point.x*scalingRatio + POINT_RAD + PADDING),
		-(_dest._point.y*scalingRatio + POINT_RAD + PADDING));
	//Lable source and dest
	RECT calcRect{};
	DrawTextA(hdc, "S", 1, &calcRect, DT_CALCRECT);
	auto width = calcRect.right - calcRect.left;
	auto height = calcRect.bottom - calcRect.top;
	calcRect.left = (_src._point.x*scalingRatio + PADDING - POINT_RAD);
	calcRect.top = -(_src._point.y*scalingRatio + PADDING + POINT_RAD + height);;
	calcRect.right = (_src._point.x*scalingRatio + PADDING + width);;
	calcRect.bottom =- (_src._point.y*scalingRatio + PADDING + POINT_RAD);;
	DrawTextA(hdc, "S", 1, &calcRect, DT_CENTER);
	DrawTextA(hdc, "D", 1, &calcRect, DT_CALCRECT);
	width = calcRect.right - calcRect.left;
	height = calcRect.bottom - calcRect.top;
	calcRect.left = (_dest._point.x*scalingRatio + PADDING - POINT_RAD);
	calcRect.top = -(_dest._point.y*scalingRatio + PADDING + POINT_RAD + height);;
	calcRect.right = (_dest._point.x*scalingRatio + PADDING + width);;
	calcRect.bottom = -(_dest._point.y*scalingRatio + PADDING + POINT_RAD);;
	DrawTextA(hdc, "D", 1, &calcRect, DT_CENTER);
	SelectObject(hdc, oldBrush);
	DeleteObject(yellowBrush);
	DeleteObject(pinkBrush);
	ReleaseDC(hWnd, hdc);
}

void Map::drawSolution(HWND hWnd)
{
	if (this->path.empty())
		return;
	HDC hdc = GetDC(hWnd);
	RECT rect;
	GetClientRect(hWnd, &rect);
	SetViewportOrgEx(hdc, 0, rect.bottom, NULL);
	auto paddingMaxX = maxX + RATIO*maxX ;
	auto paddingMaxY = maxY + RATIO*maxY ;
	
	auto redBrush = CreatePen(PS_SOLID,2,RGB(255, 40, 0));
	auto oldBrush = SelectObject(hdc, redBrush);
	for (int i = 0; i < path.size() - 1; ++i)
	{
		MoveToEx(hdc, path[i]->_point.x*scalingRatio + PADDING, -(path[i]->_point.y*scalingRatio + PADDING), NULL);
		LineTo(hdc, path[i + 1]->_point.x*scalingRatio + PADDING, -(path[i + 1]->_point.y*scalingRatio + PADDING));
	}
	//Draw path cost information
	char buffer[256];
	sprintf(buffer, "Path Cost: %lf", distance);
	RECT calcRect{};
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CALCRECT);
	auto width = calcRect.right - calcRect.left;
	auto height = calcRect.bottom - calcRect.top;
	calcRect.left = (rect.right - PADDING - width);
	calcRect.top = -(rect.bottom - PADDING - height);
	calcRect.right = (rect.right - PADDING);
	calcRect.bottom = -(rect.bottom - PADDING - 3*height);
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CENTER);

	//Draw time execution information
	sprintf(buffer,"Time: %.2lf ms", execTime);
	calcRect = {};
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CALCRECT);
	width = calcRect.right - calcRect.left;
	height = calcRect.bottom - calcRect.top;
	calcRect.left = (rect.right - PADDING - width);
	calcRect.top = -(rect.bottom - PADDING - 2*height);
	calcRect.right = (rect.right - PADDING);
	calcRect.bottom = -(rect.bottom - PADDING - 4 * height);
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CENTER);
	SelectObject(hdc, oldBrush);
	DeleteObject(redBrush);
}

void Map::drawCoordinate(HWND hWnd)
{
	
	HDC hdc = GetDC(hWnd);
	RECT rect;
	GetClientRect(hWnd, &rect);
	SetViewportOrgEx(hdc, 0, rect.bottom, NULL);
	auto bigPen = CreatePen(PS_SOLID, 2, RGB(0, 43, 198));
	auto oldPen = SelectObject(hdc, bigPen);
	//Draw column and row 
	MoveToEx(hdc, rect.left + PADDING, -(rect.top + PADDING), NULL);
	LineTo(hdc, rect.left + PADDING, -(rect.bottom + PADDING));
	MoveToEx(hdc, rect.left + PADDING, -(rect.top + PADDING), NULL);
	LineTo(hdc, rect.right + PADDING, -(rect.top + PADDING));
	//Draw arrow X 
	MoveToEx(hdc, rect.right , -(rect.top + PADDING), NULL);
	LineTo(hdc, rect.right - COOR_ARROW_X, -(rect.top + PADDING + COOR_ARROW_Y) );
	MoveToEx(hdc, rect.right, -(rect.top + PADDING), NULL);
	LineTo(hdc, rect.right - COOR_ARROW_X, (rect.top + PADDING - COOR_ARROW_Y));

	//Draw arrow Y
	
	MoveToEx(hdc, rect.left + PADDING, -(rect.bottom), NULL);
	LineTo(hdc, rect.left + PADDING - COOR_ARROW_Y, -(rect.bottom - COOR_ARROW_X));
	MoveToEx(hdc, rect.left + PADDING, -(rect.bottom), NULL);
	LineTo(hdc, rect.left + PADDING + COOR_ARROW_Y, -(rect.bottom - COOR_ARROW_X));

	int unit = std::max(maxY, maxX) / 10 + 1;
	//Draw rulerX
	for (double i = rect.left + PADDING; i <= rect.right - COOR_ARROW_X; i = i + unit*scalingRatio)
	{
		MoveToEx(hdc, i, -(rect.top + PADDING + LENGTH), NULL);
		LineTo(hdc, i, -(rect.top +PADDING - LENGTH));
	}

	//Draw Ruler Y
	for (double i = rect.top + PADDING; i <= rect.bottom - COOR_ARROW_X; i = i + unit *scalingRatio)
	{
		MoveToEx(hdc, rect.left + PADDING - LENGTH, -i, NULL);
		LineTo(hdc, rect.left + PADDING + LENGTH, -i);
	}
	char buffer[256];
	wsprintfA(buffer, "Unit: %d", unit);
	RECT calcRect{};
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CALCRECT);
	auto width = calcRect.right - calcRect.left;
	auto height = calcRect.bottom - calcRect.top;
	calcRect.left = (rect.right - PADDING - width);
	calcRect.top = -(rect.bottom - PADDING);
	calcRect.right = (rect.right - PADDING);
	calcRect.bottom = -(rect.bottom - PADDING - height);
	DrawTextA(hdc, buffer, strlen(buffer), &calcRect, DT_CENTER);
	SelectObject(hdc,oldPen);
	DeleteObject(bigPen);
}

void Map::findMaxPoint()
{
	maxX = _src._point.x;
	maxY = _src._point.y;
	if (_dest._point.x > maxX)
		maxX = _dest._point.x;
	if (_dest._point.y > maxY)
		maxY = _dest._point.y;
	if (_polyList.empty())
		return;
	for (int i = 0; i < this->_polyList.size(); ++i)
	{
		for (int j = 0; j < this->_polyList[i]->_vertList.size(); ++j)
		{
			auto polyPt = _polyList[i]->_vertList[j];
			if (polyPt->_point.x > maxX)
			{
				maxX = polyPt->_point.x;
			}
			if (polyPt->_point.y > maxY)
			{
				maxY = polyPt->_point.y;
			}

		}
	}

}

void Map::findScalingRatio(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	RECT rect;
	GetClientRect(hWnd, &rect);
	double scalingX;
	double scalingY;
	scalingX = maxX + RATIO*maxX;
	scalingY = maxY + RATIO*maxY;
	auto ratioX = (double)rect.right / scalingX;
	auto ratioY = (double)rect.bottom / scalingY;
	scalingRatio = std::min(ratioX, ratioY);
}

void Map::clearPath()
{
	path.clear();
	distance = 0;
	execTime = 0;
}



