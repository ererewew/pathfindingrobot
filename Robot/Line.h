#pragma once
#include "stdafx.h"
class POLYGON;
//Line equation format:
// Ax + By + C = 0
class Line
{
public:
	Line(const MYPOINT& a, const MYPOINT& b);
	bool setLineEquation(const MYPOINT& a, const MYPOINT& b);
	Line();
protected:
	double _coefA;
	double _coefB;
	double _coefC;
};

class LineSegment : public Line
{
public:
	const MYPOINT& getMYPOINTA();
	const MYPOINT& getMYPOINTB();
	LineSegment();
	int getRelates(const LineSegment& line, MYPOINT& result);
	int crossPolygon(const POLYGON& poly);
	bool setEndPoints(const MYPOINT& a, const MYPOINT& b);
	static LineSegment* createLineSegment(const MYPOINT& a, const MYPOINT& b);
private:
	MYPOINT _a;
	MYPOINT _b;
};