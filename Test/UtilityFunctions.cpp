#include "stdafx.h"
int CrossProduct(const POINT & a, const POINT & b, const POINT & c, const POINT & d)
{
	
		int vectXa = b.x - a.x;
		int vectYa = b.y - a.y;

		int vectXb = d.x - c.x;
		int vectYb = d.y - c.y;
		return vectXa*vectYb - vectYa*vectXb;
	
}

int DotProduct(const POINT & a, const POINT & b, const POINT & c, const POINT & d)
{
	int vectXa = b.x - a.x;
	int vectYa = b.y - a.y;

	int vectXb = d.x - c.x;
	int vectYb = d.y - c.y;
	return vectXa*vectXb + vectYa*vectYb;
}
