#pragma once
#include "stdafx.h"
#define EPSILON 0.000001
//This is where you put any functions to support computation for the project

/*
	Use to compare two real numbers
	Return 0 if equals
	Return 1 if a > b
	Return -1 if a < b
*/
template<class T>
int compare(const T& a, const T& b)
{
	if (fabs(a - b) <= EPSILON)
		return 0;
	else if (a > b)
		return 1;
	else
		return -1;
}
/*
This is utility function based on this article: https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
This function is used to calculate Cross Product between two vectors V x W.
The result is Vx * Wy - Vy * Wx
*/

int CrossProduct(const POINT& a, const POINT& b, const POINT& c, const POINT& d);

/*
This is utility function based on this article: https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
This function is used to calculate Cross Product between two vectors V x W.
The result is Vx * Wx + Vy * Wy
*/
int DotProduct(const POINT& a, const POINT& b, const POINT& c, const POINT& d);