#include <iostream>
#include "Line.h"
int main()
{
	POINT pt[] = { {1,1},{2,3},{3,5},{4,7} };
	POINT* result = new POINT{};
	LineSegment* lineSeg1 = LineSegment::createLineSegment(pt[0], pt[1]);
	LineSegment* lineSeg2 = LineSegment::createLineSegment(pt[2], pt[3]);
	auto output = lineSeg1->getRelates(*lineSeg2, result);
	if (output == 0)
		std::cout << "Intersects one point\n";
	else if (output == 1)
		std::cout << "Overlapped\n";
	else
		std::cout << "Not case above\n";

	system("pause");
	delete result;
	delete lineSeg1;
	delete lineSeg2;
}