#pragma once
#include <forward_list>
#include <list>
struct Point
{
	double x;
	double y;
};

struct Rect
{
	Point TopLeft;
	Point BottomRight;
};
struct Polygon
{
	std::list<Point> vertices; // clockwise
};

struct LineSegment
{
	Point p1;
	Point p2;
};
class Ellipse
{
public:
	Point center;
	double semimajorAxis;
	double semiminorAxis;
};