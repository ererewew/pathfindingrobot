#define _USE_MATH_DEFINES

#include "Utility.h"
#include <cmath>


double toRadian(int n)
{
	return M_PI * n / 180.0;
}

std::ofstream& operator<<(std::ofstream & os, const Point & point)
{
	os << "(";
	os << point.x << ";";
	os << point.y << ")";
	return os;
}
